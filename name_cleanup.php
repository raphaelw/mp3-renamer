<?php
require_once './helper.php';

$path = $argv[1];
$path_renamed = $argv[2];

/*
 * remove trailing slash
 */
$path = trim($path);
$path = rtrim($path,'/');

$path_renamed = trim($path_renamed);
$path_renamed = rtrim($path_renamed, '/');

/*
 * check dir exists
 */
if(!is_dir($path)) {
    echo $path . ' existiert nicht!' . "\n";
    exit();
}

if(!is_dir($path_renamed)) {
    echo $path_renamed . ' existiert nicht!' . "\n";
}



else {

    $mp3s = getAllMp3sInDirectory($path);

    $i = 0;

    foreach ($mp3s as $mp3) {

        $tag = getTags($mp3);

        $newpath = $path_renamed . '/' . cleanFilename($tag['artist']);
        if(!is_dir($newpath)) {
            mkdir($newpath);
        }
        $newpath .= '/' . cleanFilename($tag['album']);
        if(!is_dir($newpath)) {
            mkdir($newpath);
        }

        $number_of_cds = getNumberOfCds($mp3);

        $new_name = '';

        if($number_of_cds > 1) {
            if($number_of_cds < 10) {
                $number_of_cds = '0' . $number_of_cds;
            }
            $new_name = $number_of_cds . ' - ';
        }

        $new_name .= $tag['track'] . ' - ' . cleanFilename($tag['title']) . '.mp3';

        $newpath .= '/' . $new_name;

        echo $newpath."\n";

        copy($mp3, $newpath);

        $i++;
    }

    echo "\n" . $i . ' Mp3s gepurged!' . "\n";

}
