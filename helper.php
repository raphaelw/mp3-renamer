<?php
require_once('./getid3/getid3.php');
$getID3 = new getID3;
$album_infos = [];

function getFolders($path) {

    $scanned_directory = array_diff(scandir($path), array('..', '.'));

    $out = [];

    foreach ($scanned_directory as $dir) {

        if(is_dir($path . '/' . $dir)) {
            $out[] = $dir;
        }

    }

    return $out;

}

function getMp3Files($path) {
    $scanned_directory = array_diff(scandir($path), array('..', '.'));

    $out = [];

    foreach ($scanned_directory as $file) {

        $parts = explode('.', $file);
        $ext = end($parts);
        $ext = strtolower($ext);
        if(($ext === 'mp3' || 'audio/mpeg' === mime_content_type($path . '/' . $file)) && is_file($path . '/' . $file)) {
            $out[] = $file;
        }
    }

    return $out;
}

function getTags($path) {
    global $getID3;

    $tag = $getID3->analyze($path);
    $tag = $tag['tags'];

    $tmp = [];
    if(isset($tag['id3v1'])) {
        $tmp = $tag['id3v1'];
    }
    $part_of_a_set = 1;
    if(isset($tag['id3v2'])) {
        $tmp = $tag['id3v2'];
        $part_of_a_set = 1;
        if(isset($tmp['part_of_a_set'])) {
            $part_of_a_set = (int)$tmp['part_of_a_set'][0];
        }
    }

    $out = [
        'artist' => $tmp['artist'][0],
        'title' => $tmp['title'][0],
        'album' => $tmp['album'][0],
        'track' => (int)$tmp['track_number'][0],
        'part_of_a_set' => $part_of_a_set
    ];

    if($out['track'] < 10) {
        $out['track'] = '0' . $out['track'];
    }
    $out['track'] = '' . $out['track'];

    if($out['part_of_a_set'] < 10) {
        $out['part_of_a_set'] = '0' . $out['part_of_a_set'];
    }
    $out['part_of_a_set'] = '' . $out['part_of_a_set'];

    return $out;
}

function delete_directory($dirname) {
    if (is_dir($dirname))
        $dir_handle = opendir($dirname);
    if (!$dir_handle)
        return false;
    while($file = readdir($dir_handle)) {
        if ($file != "." && $file != "..") {
            if (!is_dir($dirname."/".$file))
                unlink($dirname."/".$file);
            else
                delete_directory($dirname.'/'.$file);
        }
    }
    closedir($dir_handle);
    rmdir($dirname);
    return true;
}

function getAllMp3sInDirectory($path) {

    $all_files = getDirectoryRecursive($path);

    $out = [];

    foreach ($all_files as $file) {

        $ext = substr(strtolower($file), -4);
        if($ext === '.mp3' || 'audio/mpeg' === mime_content_type($file)) {
            $out[] = $file;
        }
    }

    return $out;

}

function getDirectoryRecursive($dir, &$results = []) {
    $files = scandir($dir);

    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
        if (!is_dir($path)) {
            $results[] = $path;
        } else if ($value != "." && $value != "..") {
            getDirectoryRecursive($path, $results);
            $results[] = $path;
        }
    }

    return $results;
}

function getNumberOfCds($mp3_file) {

    global $album_infos;

    $album_path = dirname($mp3_file);

    if(isset($album_infos[$album_path])) {
        return $album_infos[$album_path]['number_of_cds'];
    }

    $mp3s = getAllMp3sInDirectory($album_path);

    $number_of_cds = 1;

    foreach ($mp3s as $mp3) {
        $tag = getTags($mp3);

        $cd_number = (int)ltrim($tag['part_of_a_set'], '0');

        if($cd_number > $number_of_cds) {
            $number_of_cds = $cd_number;
        }
    }

    $album_infos[$album_path] = [
        'number_of_cds' => $number_of_cds
    ];

    return $number_of_cds;

}

function cleanFilename($file) {

    $file = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $file);
// Remove any runs of periods (thanks falstro!)
    $file = mb_ereg_replace("([\.]{2,})", '', $file);

    return $file;

}