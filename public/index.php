<?php

$data = file_get_contents('../data.ser');

if(empty($data)) {
  $data = [];
}
else {
  $data = unserialize($data);
}

if(isset($_GET['a'])) {
    if($_GET['a'] === 'del') {
      unset($data[$_GET['k']]);
      file_put_contents('../data.ser', serialize($data));
      header('Location: /');
    }
    else if ($_GET['a'] === 'ref') {
        $data[$_GET['key']]['status'] = 0;
        file_put_contents('../data.ser', serialize($data));
        header('Location: /');
    }
}

if(isset($_POST['add'])) {
   if( isset($data[$_POST['add']])) {
     header('Location: /');
     exit();
   }
   $data = array_merge([
           $_POST['add'] => [
               'status' => 0
           ]
       ],
        $data
   );

   file_put_contents('../data.ser', serialize($data));
   header('Location: /');
}

$list = '<ul>';

foreach($data as $key => $value) {
  $list .= '<li>' . $key . ' (' . $value['status'] . ') <a href="/?a=del&k=' . urlencode($key) . '">[ löschen ]</a> <a href="/?a=ref&k=' . urlencode($key) . '">[ reset ]</a></li>';
}

$list .= '</ul>';


?><html>
<head>
<title>Spotify Ripper</title>
</head>
<body>
<p>
<form method="post">
Spotify URL hinzufügen<br>
<input type="text" name="add" id="add"><button>GO</button>
</form>
</p>
<hr>
<div id="list"><?= $list; ?></div>
</body>
</html>
