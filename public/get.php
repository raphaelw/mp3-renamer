<?php

$data = file_get_contents('../data.ser');

if(empty($data)) {
  $data = [];
}
else {
  $data = unserialize($data);
}

foreach($data as $spotify_uri => $value) {

   if($value['status'] === 0) {
     $value['status'] = 1;
     $data[$spotify_uri] = $value;
     file_put_contents('../data.ser', serialize($data));
     echo $spotify_uri;
     exit();
   }

}

echo 'NIX';
exit();
