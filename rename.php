<?php
require_once './helper.php';

$path = $argv[1];

if(!is_dir($path)) {
    echo $path . ' existiert nicht!';
    exit();
}

$mp3s = getAllMp3sInDirectory($path);

foreach ($mp3s as $mp3) {

    $tag = getTags($mp3);

    $number_of_cds = getNumberOfCds($mp3);

    $new_name = '';

    if($number_of_cds > 1) {
        if($number_of_cds < 10) {
            $number_of_cds = '0' . $number_of_cds;
        }
        $new_name = $number_of_cds . ' - ';
    }

    $new_name .= $tag['track'] . ' - ' . cleanFilename($tag['title']) . '.mp3';

    $path = dirname($mp3);

    rename($mp3, $path . '/' . $new_name);
    echo basename($mp3) . ' => ' . $new_name . "\n";
}
